CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

The Views Menu Item Reference module provides a contextual filter default value
which return current active menu link form given menu and using that you can
filter your content for current active menu link.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/views_menu_item_reference

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/views_menu_item_reference

REQUIREMENTS
------------

This module requires the following modules:

 * Views (Drupal Core)
 * Custom Menu Links (Drupal Core)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Go to view and click on add contextual filters under advance section select
   any menu item referance field and provide it's default value form current
   active menu item. in configuration provide which menu you want to use for
   menu item selection.
