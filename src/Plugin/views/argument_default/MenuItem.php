<?php

namespace Drupal\views_menu_item_reference\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Menu\MenuLinkManager;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\MenuStorage;

/**
 * Default argument plugin to extract a menu item.
 *
 * @ViewsArgumentDefault(
 *   id = "menu_item",
 *   title = @Translation("Current menu item id")
 * )
 */
class MenuItem extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The menu link service.
   *
   * @var \Drupal\Core\Menu\MenuLinkManager
   */
  protected $menuLinkService;

  /**
   * The menu.
   *
   * @var \Drupal\system\MenuStorage
   */
  protected $menuStorage;

  /**
   * Constructs a new Node instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, MenuLinkManager $menu_link_service, MenuStorage $menu_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->routeMatch = $route_match;
    $this->menuLinkService = $menu_link_service;
    $this->menuStorage = $menu_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('plugin.manager.menu.link'),
      $container->get('entity.manager')->getStorage('menu')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['menu'] = ['default' => 'main'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $menus = $this->menuStorage->loadMultiple();
    $menuOptions = [];
    foreach ($menus as $menuEntity) {
      $menuOptions[$menuEntity->id()] = $menuEntity->label();
    }
    $form['menu'] = [
      '#type' => 'select',
      '#title' => $this->t('Source Menu'),
      '#options' => $menuOptions,
      '#default_value' => $this->options['menu'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() { 
    $routeName = $this->routeMatch->getRouteName();
    $routeParameters = $this->routeMatch->getRawParameters();
    $menu = $this->options['menu'];
    $menuLink = $this->menuLinkService->loadLinksByRoute($routeName, $routeParameters->all(), $menu);
    if (!empty($menuLink)) {
      $menuLink = reset($menuLink);
      $menuLinkParameters = $menuLink->getEditRoute()->getRouteParameters();
      return $menuLinkParameters['menu_link_content'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

}
